//
//  RequestHandler.swift
//  Artery
//
//  Created by Jatin Garg on 15/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation

enum RequestType: String{
    case get = "GET"
    case post = "POST"
}


class RequestHandler: NSObject{
    static let shared = RequestHandler()
    
    func makeRequest(with url: URL?,using parameters: [String:Any]?,of type: RequestType,_ completion: (([String:Any]?,Error?)->Void)?){
        guard  var requestURL = url else {
            let error = NSError(domain: "Provide url", code: 404, userInfo: nil)
            completion?(nil,error as Error)
            return
        }
        
        
        if type == .get && parameters != nil{
            let paramString = parameters!.stringFromHttpParameters()
            requestURL = URL(string: requestURL.absoluteString + "?\(paramString)")!
        }
        
        let session = URLSession.shared
        var request = URLRequest(url: requestURL)
        
        if parameters != nil && type == .post{
            var jsonData: Data!
            request.httpMethod = "POST"
            do{
                jsonData = try JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
            }catch let error{
                completion?(nil,error)
                return
            }
            request.httpBody = jsonData
        }
        if type == .get{
            request.httpMethod = "GET"
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if error != nil{
                completion?(nil,error)
                return
            }
            if data == nil{
                completion?(nil,NSError(domain: "Couldn't donwload data", code: 404, userInfo: nil) as Error)
                return
            }
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                if json as? [String:Any] == nil{
                    let error = NSError(domain: "Type casting failed", code: 404, userInfo: nil)
                    completion?(nil,error)
                }else{
                    completion?(json as? [String:Any],nil)
                }
                
            }catch let jsonerror {
                completion?(nil,jsonerror)
                return
            }
        }
        task.resume()
    }
}
