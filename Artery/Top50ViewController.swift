//
//  Top50ViewController.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

enum Top50What{
    case artist
    case artworks
}

class Top50ViewController: UIViewController{
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var currencySwitch: UISwitch!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var controllerType: Top50What = .artist
    var topArtists: [ArtistModel]?
    var topArtWorks: [ArtworkModel]?
    var currency: String!
    
    @IBAction func currencyFlipped(_ sender: Any) {
        let s = sender as! UISwitch
        if s.isOn{
            currency = "INR"
        }else{
            currency = "USD"
        }
        getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if currencySwitch.isOn{
            currency = "INR"
        }else{
            currency = "USD"
        }
        backBtn.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        getData()
    }
    
    func backTapped(){
        _=navigationController?.popViewController(animated: true)
    }
    
    var currentPage: Int = 0
    var canLoadMore = true
    
    func getData(){
        CircularProgressView.startAnimating()
        switch controllerType {
        case .artist:
            headerImage.image = #imageLiteral(resourceName: "top50.png")
            ArtistModel.getTop50Artists(usingCurrency: currency, completion: { (model) in
                CircularProgressView.stopAnimating()
                self.topArtists = model
                OperationQueue.main.addOperation {
                    self.tableView.reloadData()
                }
            })
        case .artworks:
            headerImage.image = #imageLiteral(resourceName: "top500.png")
            ArtworkModel.fetchTop500Works(forCurrency: currency, page: currentPage, completion: { (model) in
                CircularProgressView.stopAnimating()
                
                if let artWorks = model{
                    if self.currentPage == 0{
                        self.topArtWorks = artWorks
                    }else{
                        artWorks.forEach({ (work) in
                            self.topArtWorks?.append(work)
                        })
                    }
                    
                    self.currentPage += 1
                    OperationQueue.main.addOperation {
                        self.tableView.reloadData()
                    }
                }else{
                    self.canLoadMore = false
                }
                
                
                
            })
            
        }
    }
}

extension Top50ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if topArtists != nil{
            return topArtists!.count
        }else if topArtWorks != nil{
            return topArtWorks!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "top50") as! Top50ArtistTVC
        if let artists = topArtists{
            cell.backImage.sd_setImage(with: URL(string: artists[indexPath.row].mugshot ?? ""))
            cell.rankView.rank.text = "\(indexPath.row + 1)"
        }
        if let works = topArtWorks{
            cell.backImage.sd_setImage(with: URL(string: works[indexPath.row].thumbImage ?? ""))
            cell.rankView.rank.text = "\(indexPath.row + 1)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch controllerType {
        case .artist:
            break
        default:
            //artworks
            if indexPath.row == topArtWorks!.count - 1 && canLoadMore {
                //load more arts
                getData()
            }
        }
    }
}
