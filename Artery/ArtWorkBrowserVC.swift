//
//  ArtWorkBrowserVC.swift
//  Artery
//
//  Created by Jatin Garg on 20/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

var artWorkBrowserCellID = "artWorkBrowserCVC"
class ArtWorkBrowserVC: UIViewController{
    
    @IBOutlet weak var collectionView: UICollectionView!
    var myContainer: UIView!
    var selectedIndex: Int?
    
    var artWorks = [ArtworkModel]()
    var canLoadMore = true
    var currentPage: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
    }
    
    @IBAction func backTapped(_ sender: UIButton){
        _=navigationController?.popViewController(animated: true)
    }
    
    
    func getData(){
        CircularProgressView.startAnimating()
        ArtworkModel.fetchTop500Works(forCurrency: "INR", page: currentPage) { (model) in
            CircularProgressView.stopAnimating()
            if let artWorks = model{
                if self.currentPage == 0{
                    self.artWorks = artWorks
                }else{
                    artWorks.forEach({ (work) in
                        self.artWorks.append(work)
                    })
                }
                self.currentPage += 1
                OperationQueue.main.addOperation {
                    self.collectionView.reloadData()
                }
            }else{
                self.canLoadMore = false
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ArtViewController
        destination.artWork = artWorks[selectedIndex!]
    }
   
}

extension ArtWorkBrowserVC: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artWorks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: artWorkBrowserCellID, for: indexPath) as! ArtworkBrowserCVC
        let artWork = artWorks[indexPath.item]
        cell.artwork = artWork
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = UIScreen.main.bounds.width/3
        let width = height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.artWorks.count-1 && canLoadMore{
            //time to load more
            getData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        performSegue(withIdentifier: "artview", sender: self)
    }
    
}
