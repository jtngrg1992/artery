//
//  BrowseTVC.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class BrowseTVC: UITableViewCell {

    @IBOutlet weak var goto: UIButton!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var backImage: JGImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        goto.layer.borderColor = primaryAccentColor.cgColor
        goto.layer.borderWidth = 2
        goto.setFAIcon(icon: .FAAngleDown, forState: .normal)
        goto.tintColor = primaryAccentColor
        goto.backgroundColor = .white
        let titleString = itemTitle.text
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 3
        shadow.shadowColor = UIColor.black
        shadow.shadowOffset  = CGSize(width: 1, height: 1)
        
//        itemTitle.font = UIFont(name: "Futura Condensed Regular", size: 25)
        let attrText = NSAttributedString(string: titleString!, attributes: [NSShadowAttributeName:shadow])
        itemTitle.attributedText = attrText
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
