//
//  BrowseTVC.swift
//  Artery
//
//  Created by Jatin Garg on 23/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class BrowseTableViewCell: UITableViewCell{
    
    @IBOutlet weak var gotoBtn: UIButton!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var backImage: JGImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        gotoBtn.layer.borderColor = primaryAccentColor.cgColor
        gotoBtn.layer.borderWidth = 1
        gotoBtn.setFAIcon(icon: .FAAngleDown, forState: .normal)
        gotoBtn.tintColor = primaryAccentColor
        
        let titleString = itemTitle.text
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 3
        shadow.shadowColor = UIColor.black
        shadow.shadowOffset  = CGSize(width: 1, height: 1)
        
        //itemTitle.font = UIFont(name: "BebasNeue", size: 25)
        let attrText = NSAttributedString(string: titleString!, attributes: [NSShadowAttributeName:shadow])
        itemTitle.attributedText = attrText
    }
}
