//
//  CircularProgress.swift
//  ProfilePage
//
//  Created by Shrofile on 06/07/17.
//  Copyright © 2017 Shrofile. All rights reserved.
//

import UIKit

class CircularProgressView: UIView{
    
    var progressLabel: UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.systemFont(ofSize: 10)
        l.textColor = .white
        l.textAlignment = .center
        l.numberOfLines = 3
        return l
    }()
    
    var value: CGFloat = 0{
        didSet{
            progressLabel.text = "\(Int(value*100))%"
            updateProgress(with: value)
        }
    }
    
    var isSpinning = false{
        didSet{
            if !isSpinning{
                progressLabel.text = ""
            }
        }
    }
    var fillColor: UIColor = .red
    var arcColor : UIColor = .clear
    
    var offset: CGFloat = 0
    
    
    var boundaryWidth: CGFloat = 1
    
    
    
    
    var fillWidth: CGFloat = 5
    let boundaryLayer = CAShapeLayer()
    let fillLayer = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        backgroundColor = .clear
        addSubview(progressLabel)
        progressLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        progressLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        progressLabel.widthAnchor.constraint(equalToConstant: 2*bounds.width).isActive = true
        
        let radius: CGFloat = min(bounds.width/2, bounds.height/2) - offset/2
        let arcCenter = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let boundaryArc = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        boundaryLayer.strokeColor = arcColor.cgColor
        boundaryLayer.path = boundaryArc.cgPath
        boundaryLayer.lineWidth = boundaryWidth
        boundaryLayer.fillColor = UIColor.clear.cgColor
        boundaryLayer.frame = bounds
        layer.addSublayer(boundaryLayer)
        
        fillLayer.strokeColor = fillColor.cgColor
        fillLayer.lineCap = kCALineCapRound
        fillLayer.lineWidth = fillWidth
        fillLayer.fillColor = UIColor.clear.cgColor
        fillLayer.frame = bounds
        layer.addSublayer(fillLayer)
    }
    
    func updateProgress(with value: CGFloat){
        let radius: CGFloat = min(bounds.width/2, bounds.height/2) - offset/2
        let arcCenter = CGPoint(x: bounds.width/2, y: bounds.height/2)
        let endAngle: CGFloat = value * 2 * CGFloat.pi
        let fillPathInnerArc = UIBezierPath(arcCenter: arcCenter, radius: radius, startAngle: 0, endAngle: endAngle, clockwise: true)
        fillLayer.path = fillPathInnerArc.cgPath
    }
    
    func start(){
        if isSpinning{
            return
        }
        isSpinning = true
        //update fillLayer's path and add  animation
        updateProgress(with: 0.8)
        let rotationAnimation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation.toValue = NSNumber(value: Double.pi * 2.0)
        rotationAnimation.duration = 1;
        rotationAnimation.isCumulative = true;
        rotationAnimation.repeatCount = HUGE;
        OperationQueue.main.addOperation {
            self.fillLayer.add(rotationAnimation, forKey: nil)
        }
        
    }
    
    static let loader = CircularProgressView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
    
    class func startAnimating(){
        if let keyWindow = UIApplication.shared.keyWindow{
            let v = UIView(frame: .zero)
            v.frame = keyWindow.bounds
            v.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            loader.translatesAutoresizingMaskIntoConstraints = false
            keyWindow.addSubview(v)
            v.addSubview(loader)
            loader.centerXAnchor.constraint(equalTo: v.centerXAnchor).isActive = true
            loader.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
            loader.heightAnchor.constraint(equalToConstant: 50).isActive = true
            loader.widthAnchor.constraint(equalToConstant: 50).isActive = true
            loader.start()
        }
    }
    
    class func stopAnimating(){
        OperationQueue.main.addOperation {
            if self.loader.isSpinning{
                if let overlay = self.loader.superview{
                    self.loader.stop()
                    overlay.removeFromSuperview()
                }
            }
        }
        
    }
    
    func stop(){
        if !isSpinning{
            return
        }
        isSpinning = false
        fillLayer.removeAllAnimations()
        updateProgress(with: self.value)
    }
}
