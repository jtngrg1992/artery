//
//  ArtworkBrowserCVC.swift
//  Artery
//
//  Created by Jatin Garg on 20/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Font_Awesome_Swift
class ArtworkBrowserCVC: UICollectionViewCell {

    @IBOutlet weak var likeButton: BubblyButton!
    @IBOutlet weak var imageView: JGImageView!
    
    var artwork: ArtworkModel?{
        didSet{
            configure()
        }
    }
    
    
    private func configure(){
        reset()
        
        guard artwork != nil else { return }
        
        imageView.imageURL = artwork!.thumbImage
        if artwork!.isLiked {
            likeButton.tintColor = .red
        }else{
            likeButton.tintColor = primaryBtnTint
        }
    }
    
    private func reset(){
        //remove prev image and reset like status
        imageView.image = nil
        likeButton.tintColor = primaryBtnTint
    }
    
    @IBAction func likeBtnTapped( _ sender: UIButton){
        artwork?.toggleLike(){(success,message) in
            DispatchQueue.main.async {
                if success{
                    //like success
                    UIView.animate(withDuration: 0.3, animations: {
                        let tintColor = self.artwork!.isLiked ? primaryBtnTint : .red
                        self.likeButton.tintColor = tintColor
                        self.artwork!.isLiked = !self.artwork!.isLiked
                        
                    })
                }else{
                    //like failure
                    iToast.makeText("The action couldn't be performed, please try again later").show()
                    
                }
            }
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        likeButton.tintColor = primaryBtnTint
        likeButton.setFAIcon(icon: .FAHeart, forState: .normal)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
