//
//  RankView.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class RankView: UIView{
    @IBOutlet weak var rank: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        let nib = Bundle.main.loadNibNamed("RankView", owner: self, options: nil)?.first as! UIView
        nib.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        nib.frame = bounds
        addSubview(nib)
        
        let shape = CAShapeLayer()
        shape.frame = bounds
        shape.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.bottomLeft,.bottomRight], cornerRadii: CGSize(width: 3, height: 3)).cgPath
        layer.mask = shape
    }
}
