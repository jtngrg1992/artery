//
//  DashboardHomeVC.swift
//  Artery
//
//  Created by Jatin Garg on 30/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class DashboardHomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    var images = [String]()
    var items = ["Performance\nTracker", "Upcoming Works", "Artists Followed", "Paintings Liked", "Saved Insights"]
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var likedPaintings: [ArtworkModel]?
    var followedArtists: [ArtistModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "BrowseTVC", bundle: nil), forCellReuseIdentifier: "browseTVC")
        backBtn.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
    }
    
    func backTapped(){
        _=navigationController?.popViewController(animated: true)
    }
    
    func getRandomWork()->UIImage?{
        let images = Bundle.main.paths(forResourcesOfType: "jpg", inDirectory: nil)
        let random = arc4random_uniform(UInt32(images.count))
        if let data = try? Data(contentsOf: URL(fileURLWithPath: images[Int(random)])), let image = UIImage(data: data){
            if self.images.index(of: images[Int(random)]) != nil{
                return getRandomWork()
            }
            self.images.append(images[Int(random)])
            return image
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "browseTVC") as! BrowseTVC
        cell.itemTitle.text = items[indexPath.row]
        cell.backImage.image = getRandomWork()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = ((UIScreen.main.bounds.height-64) - (CGFloat(items.count-1)*minLineSpacing))/3
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItem = items[indexPath.row]
        
        if selectedItem?.lowercased() == "paintings liked" {
            CircularProgressView.startAnimating()
            userModel?.getLikedPaintings{ paintings in
                DispatchQueue.main.async {
                    CircularProgressView.stopAnimating()
                    guard let paintings = paintings else {
                        iToast.makeText("Error encountered while fetching liked paintings").show()
                        return
                    }
                    self.likedPaintings = paintings
                    self.performSegue(withIdentifier: "liked", sender: self)
                }
            }
        }else if selectedItem?.lowercased() == "artists followed"{
            CircularProgressView.startAnimating()
            userModel?.getFollowedArtists {artists in
                DispatchQueue.main.async {
                    CircularProgressView.stopAnimating()
                    guard let artists = artists else{
                        iToast.makeText("Error encounted while fetching followed artists").show()
                        return
                    }
                    
                    self.followedArtists = artists
                    self.performSegue(withIdentifier: "liked", sender: self)
                }
            }
        }
    }
    
    var selectedItem: String?
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: self)
        
        if let gridVC = segue.destination as? GridCollectionViewController{
            if selectedItem?.lowercased() == "paintings liked"{
                gridVC.likedPaintings = self.likedPaintings
                gridVC.displayMode = .likedPaintings
                
            }else if selectedItem?.lowercased() == "artists followed"{
                gridVC.followedArtists = self.followedArtists
                gridVC.displayMode = .followedArtists
            }
        }
    
    }
}
