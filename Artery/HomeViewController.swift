//
//  HomeViewController.swift
//  Artery
//
//  Created by Jatin Garg on 19/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController{
    
    @IBOutlet weak var tabStack: UIStackView!
    @IBOutlet weak var headerTab: UIView!
    @IBOutlet weak var notificationsBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var collectionsBtn: TabBarButton!
    @IBOutlet weak var dashboardBtn: TabBarButton!
    @IBOutlet weak var browseBtn: TabBarButton!
    
    let browserVC: UIViewController = {
        let sb = UIStoryboard(name: "Browse", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        return vc!
    }()
    let dashboardVC: UIViewController = {
        let sb = UIStoryboard(name: "Dashboard", bundle: nil)
        let vc = sb.instantiateInitialViewController()
        return vc!
    }()
    
    var children = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerTab.layer.shadowOpacity = 0.6
        headerTab.layer.shadowOffset = CGSize(width: 0, height: 4)
        headerTab.layer.masksToBounds = false
        headerTab.layer.shadowRadius = 3
    
        browseBtn.isSelectedItem = true
        let sender = tabStack.arrangedSubviews[1] as! TabBarButton
        sender.setTitle("Browse", for: .normal)
        tabButtonClicked(sender)
    }
    
    @IBAction func tabButtonClicked(_ sender: TabBarButton) {
        for subView in tabStack.arrangedSubviews.filter({$0 as? TabBarButton != nil}){
            (subView as! TabBarButton).isSelectedItem = false
        }
        sender.isSelectedItem = true
        
        switch sender.titleLabel!.text!.trimmingCharacters(in: .whitespacesAndNewlines) {
        case "Browse":
            removeOldChildren()
            add(asChildViewController: browserVC)
            children.append(browserVC)
        case "My Collection":break
        case "Dashboard":
            removeOldChildren()
            add(asChildViewController: dashboardVC)
            children.append(dashboardVC)
        default:break
            
        }}
    
    private func removeOldChildren(){
        for vc in children{
            remove(asChildViewController: vc)
            children.remove(at: children.index(of: vc)!)
        }
    }
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }
}


