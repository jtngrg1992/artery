//
//  ArtistMainViewController.swift
//  Artery
//
//  Created by Jatin Garg on 23/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Font_Awesome_Swift

class ArtistMainViewController: UIViewController{
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var artistImage: JGImageView!
    @IBOutlet weak var worksLabel: UILabel!
    @IBOutlet weak var worksBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var artistName: UILabel!
    
    var artistID: Int?
    var artist: ArtistModel?{
        didSet{
            if artist != nil{
                OperationQueue.main.addOperation {
                    self.fillSubviews()
                }
                
            }else{
                _=kCommonFunctions.handledError(result: nil, error: NSError(domain: "Invalid response from server", code: 404, userInfo: nil) as Error)
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubs()
        if artistID != nil{
            CircularProgressView.startAnimating()
            ArtistModel.getArtist(withID: artistID!, completion: {
                (artistModel) in
                CircularProgressView.stopAnimating()
                self.artist = artistModel
                
            })
        }
    }
    
    
    func fillSubviews(){
        artistImage.imageURL = artist?.mugshot
        artistName.text = artist?.name ?? ""
        category.text = artist?.categoryName ?? ""
        category.backgroundColor = hexStringToUIColor(hex: artist?.categoryColor ?? "")
        if let rank = artist?.rank{
            let rankView = UIView(frame: .zero)
            rankView.backgroundColor = primaryAccentColor
            let headerLabel = UILabel()
            headerLabel.text = "RANK"
            headerLabel.font = UIFont(name: "Hero", size: 18)
            headerLabel.translatesAutoresizingMaskIntoConstraints = false
            headerLabel.textColor = .white
            rankView.addSubview(headerLabel)
            let rankLabel = UILabel()
            rankLabel.text = "\(rank)"
            rankLabel.font = UIFont(name: "Hero", size: 40)
            rankLabel.textColor = .white
            rankLabel.translatesAutoresizingMaskIntoConstraints = false
            rankView.addSubview(rankLabel)
            
            headerLabel.topAnchor.constraint(equalTo: rankView.topAnchor, constant: 5).isActive = true
            headerLabel.centerXAnchor.constraint(equalTo: rankView.centerXAnchor).isActive = true
            rankLabel.centerXAnchor.constraint(equalTo: rankView.centerXAnchor).isActive = true
            rankLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 5).isActive = true
            
            rankView.translatesAutoresizingMaskIntoConstraints = false
            
            artistImage.addSubview(rankView)
            rankView.topAnchor.constraint(equalTo: artistImage.topAnchor,constant: 2).isActive = true
            rankView.leftAnchor.constraint(equalTo: artistImage.leftAnchor, constant: 2).isActive = true
            rankView.widthAnchor.constraint(equalToConstant: 55).isActive = true
            rankView.heightAnchor.constraint(equalToConstant: 70).isActive = true
            rankView.layer.masksToBounds = true
            rankView.layer.cornerRadius = 4
        }
        
        //category.backgroundColor = UIColor(rgb: Int(artist!.categoryColor!)!)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func setupSubs(){
        worksLabel.font = UIFont(name: "BebasNeue", size: 20)
        
        worksBtn.layer.borderColor = primaryAccentColor.cgColor
        worksBtn.layer.borderWidth = 4
        //worksBtn.titleLabel?.textColor = primaryAccentColor
        worksBtn.setFAIcon(icon: .FAAngleDown, iconSize: 25, forState: .normal)
        worksBtn.tintColor = primaryAccentColor
        
        likeBtn.setFAIcon(icon: .FAHeart, iconSize: 30, forState: .normal)
        likeBtn.tintColor = primaryAccentColor
        
        shareBtn.setFAIcon(icon: .FAShareAlt, iconSize: 30, forState: .normal)
        shareBtn.tintColor = primaryAccentColor
    }
    
    @IBAction func likeTapped(_ sender: UIButton) {
    }
    
    
    @IBAction func shareTapped(_ sender: UIButton) {
    }
    
    @IBAction func worksBtnPressed(_ sender: UIButton) {
        //performSegue(withIdentifier: "artistWorks", sender: self)
        let containerView = self.view.superview
        let worksVC = storyboard!.instantiateViewController(withIdentifier: "artistWorks") as! ArtistWorksViewController
        navigationController?.addChildViewController(worksVC)
        worksVC.delegate = self
        worksVC.artist = self.artist
        worksVC.view.frame = containerView!.bounds
        worksVC.view.transform = CGAffineTransform(translationX: 0, y: (containerView?.frame.height)!)
        containerView?.addSubview(worksVC.view)
        worksVC.didMove(toParentViewController: self)
        UIView.animate(withDuration: 0.4, animations: {
            self.view.transform = CGAffineTransform(translationX: 0, y: -(containerView?.frame.height)!)
            worksVC.view.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    func childVCDismissed(vc: UIViewController){
        let containerView = self.view.superview!
        UIView.animate(withDuration: 0.4, animations: {
            self.view.transform = CGAffineTransform.identity
            vc.view.transform = CGAffineTransform(translationX: 0, y: containerView.frame.height)
        }) { (success) in
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
        }
        
    }
    @IBAction func backtapped(_ sender: Any) {
        _=navigationController?.popViewController(animated: true)
    }
    
}
