//
//  ACTViewController.swift
//  Artery
//
//  Created by Jatin Garg on 24/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreGraphics

class ACTViewController: UIViewController{
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    var items = ["ARTERY ORGANISER","ARTIST PRICE DATABASE","ARTERY RANKINGS", "ARTIST REPORTS"]
    
    var images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "BrowseTVC", bundle: nil)
            , forCellReuseIdentifier: "browseTVC")
        tableView.estimatedRowHeight = 300
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        _=navigationController?.popViewController(animated: true)
    }
    
    func getRandomWork()->UIImage?{
        let images = Bundle.main.paths(forResourcesOfType: "jpg", inDirectory: nil)
        let random = arc4random_uniform(UInt32(images.count))
        if let data = try? Data(contentsOf: URL(fileURLWithPath: images[Int(random)])), let image = UIImage(data: data){
            if self.images.index(of: images[Int(random)]) != nil{
                return getRandomWork()
            }
            self.images.append(images[Int(random)])
            return image
        }
        return nil
    }
}

extension ACTViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = ((UIScreen.main.bounds.height-64) - (CGFloat(items.count-1)*minLineSpacing))/3
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "browseTVC") as! BrowseTVC
        if images.indices.contains(indexPath.row){
            let url = URL(fileURLWithPath: images[indexPath.row])
            let data = try? Data(contentsOf: url)
            let image = UIImage(data: data!)
            cell.backImage.image = image
        }else{
            cell.backImage.image = getRandomWork()
        }
        
        cell.itemTitle.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            performSegue(withIdentifier: "pdb", sender: self)
        }
        if indexPath.row == 2{
            performSegue(withIdentifier: "rankings", sender: self)
        }
        
    }
    
}
