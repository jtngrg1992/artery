//
//  RankingsViewController.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class RankingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    let items = ["TOP 500 WORKS","TOP 50 ARTISTS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
            headingLabel.font = UIFont(name: "BebasNeue", size: 22)

        // Do any additional setup after loading the view.
    }

    @IBAction func backTapped(_ sender: Any) {
         _=navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var selectedCategory: Top50What!
}

extension RankingsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "browseTVC") as! BrowseTVC
        var imgUrl: String!
        if indexPath.row == 0{
            imgUrl = Bundle.main.path(forResource: "top500", ofType: "png")
        }else{
            imgUrl = Bundle.main.path(forResource: "top50", ofType: "png")
        }
        let data = try? Data(contentsOf: URL(fileURLWithPath: imgUrl))
        let image = UIImage(data: data!)
        cell.itemTitle.text = items[indexPath.row]
        cell.backImage.image = image
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = ((UIScreen.main.bounds.height-64) - (CGFloat(items.count-1)*minLineSpacing))/2
        return height - 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            selectedCategory = .artworks
            performSegue(withIdentifier: "top50", sender: self)
        }
        if indexPath.row == 1{
            selectedCategory = .artist
            performSegue(withIdentifier: "top50", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! Top50ViewController
        destination.controllerType = selectedCategory
    }
}
