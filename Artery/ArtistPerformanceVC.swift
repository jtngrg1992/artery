//
//  ArtistPerformanceVC.swift
//  Artery
//
//  Created by Jatin Garg on 09/09/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class ArtistPerformanceVC: UIViewController{
    
    @IBOutlet weak var artistBtn: UIButton!
    @IBOutlet weak var collectiveBtn: UIButton!
    @IBOutlet weak var auctionHouseBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
