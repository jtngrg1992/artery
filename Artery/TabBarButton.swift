//
//  TabButton.swift
//  Artery
//
//  Created by Jatin Garg on 20/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class TabBarButton: UIButton{
    var isSelectedItem: Bool = false{
        didSet{
            setup()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        if isSelectedItem{
            tintColor = primaryAccentColor
        }else{
            tintColor = primaryBtnTint
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.7, options: [.curveEaseInOut,.allowUserInteraction], animations: { 
            self.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (success) in
            
        }
        isSelectedItem = !isSelectedItem
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.7, options: [.curveEaseInOut,.allowUserInteraction], animations: {
            self.transform = CGAffineTransform.identity
        }) { (success) in
            
        }
    }
}
