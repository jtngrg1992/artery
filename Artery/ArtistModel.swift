//
//  ArtistModel.swift
//  Artery
//
//  Created by Jatin Garg on 23/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation

struct ArtistModel{
    var yob: Int?
    var id: Int?
    var yod: String?
    var name: String?
    var mugshot: String?
    var category: String?
    var categoryColor: String?
    var categoryName: String?
    var totalUSD: Double?
    var worksSold: Int?
    var rank: Int?
    
    init(dict: [String:Any]) {
        yob = dict["yob"] as? Int
        yod = dict["yod"] as? String
        id = dict["artist_id"] as? Int
        name = dict["artist_name"] as? String
        mugshot = dict["mugshot"] as? String
        category = dict["artist_category"] as? String
        categoryColor = dict["artist_category_color"] as? String
        totalUSD = dict["total_usd"] as? Double
        categoryName = dict["artist_category_name"] as? String
        worksSold = dict["works_sold"] as? Int
        rank = dict["rank"] as? Int
    }
    
    typealias followCompletion = (_ success: Bool) -> Void
    
    func toggleFollow(_ completion: followCompletion?){
        let url = kBaseURL + kFollowUnfollow
        
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token" : userModel?.accessToken ?? "", "artist_id" : self.id ?? ""], of: .post) { (result, error) in
            if error != nil {
                completion?(false)
            }else{
                CommonFunctions.shared.debugPrint(dict: result!)
            }
        }
    }
    
    
    static func getArtist(withID id: Int, completion: @escaping (ArtistModel?)->Void){
        RequestHandler.shared.makeRequest(with: URL(string: kBaseURL+kartistInfo ), using: ["access_token" : userModel?.accessToken ?? "", "artist_id": id ], of: .post) { (result, error) in
            OperationQueue.main.addOperation {
                if !kCommonFunctions.handledError(result: result, error: error){
                    var data = result?["data"] as? [String:Any]
                    RequestHandler.shared.makeRequest(with: URL(string: kBaseURL+kArtistRank), using: ["access_token":userModel?.accessToken ?? "","artist_id": id], of: .post, { (result, error) in
                        _=kCommonFunctions.handledError(result: result, error: error)
                        let rank = (result?["data"] as? [String:Any])?["rank"] as? Int
                        data?["rank"] = rank
                        completion(ArtistModel(dict: data!))
                    })
                }else{
                    completion(nil)
                }
            }
            
        }
    }
    
    static func getTop50Artists(usingCurrency currency: String,completion: @escaping ([ArtistModel]?)->Void){
        RequestHandler.shared.makeRequest(with: URL(string: kBaseURL+kTop50Artists), using: ["access_token": userModel?.accessToken ?? "", "currency" : currency], of: .post) { (result, error) in
            if !kCommonFunctions.handledError(result: result, error: error){
                guard let data = result?["data"] as? [[String:Any]] else {completion(nil);return}
                var temp = [ArtistModel]()
                for dict in data{
                    temp.append(ArtistModel(dict: dict))
                }
                completion(temp)
            }else{
                completion(nil)
            }
        }
    }
    
    static func getFeaturedArtists(completion: @escaping ([ArtistModel]?)->Void){
        RequestHandler.shared.makeRequest(with: URL(string: kBaseURL+kFeaturedArtists), using: ["access_token" : userModel?.accessToken ?? ""], of: .post) { (result, error) in
            if !kCommonFunctions.handledError(result: result, error: error){
                guard let data = result?["data"] as? [[String:Any]] else {completion(nil); return;}
                var temp = [ArtistModel]()
                for dict in data{
                    temp.append(ArtistModel(dict: dict))
                }
                completion(temp)
            }else{
                completion(nil)
            }
        }
    }

}
