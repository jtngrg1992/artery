//
//  BrowseViewController.swift
//  Artery
//
//  Created by Jatin Garg on 20/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

let browseCellID = "browseTVC"
let minLineSpacing: CGFloat = 3

class BrowseViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var items = ["BROWSE","A.C.T","DASHBOARD"]
    var images = [String]()
    var artWorks = [ArtworkModel]()
    var myContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)

    }
    func getRandomWork()->UIImage?{
        let images = Bundle.main.paths(forResourcesOfType: "jpg", inDirectory: nil)
        let random = arc4random_uniform(UInt32(images.count))
        if let data = try? Data(contentsOf: URL(fileURLWithPath: images[Int(random)])), let image = UIImage(data: data){
            if self.images.index(of: images[Int(random)]) != nil{
                return getRandomWork()
            }
            self.images.append(images[Int(random)])
            return image
        }
        return nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}

extension BrowseViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: browseCellID) as! BrowseTableViewCell
        cell.backImage.image = getRandomWork()
        cell.itemTitle.text = items[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = ((UIScreen.main.bounds.height-64) - (CGFloat(items.count-1)*minLineSpacing))/3
        return height
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            //browse
            performSegue(withIdentifier: "browseartwork", sender: self)
        case 1:
            //ACT
            let sb = UIStoryboard(name: "ACT", bundle: nil)
            if let vc = sb.instantiateInitialViewController() as? UINavigationController{
                navigationController?.pushViewController(vc.viewControllers[0], animated: true)
            }
        case 2:
            //dashboard 
            let sb = UIStoryboard(name: "Dashboard", bundle: nil)
            if let vc = sb.instantiateInitialViewController() as? UINavigationController{
                navigationController?.pushViewController(vc.viewControllers[0], animated: true)
            }
            
        default:
            break
        }
    }
    
}
