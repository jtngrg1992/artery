//
//  ArtworkModel.swift
//  Artery
//
//  Created by Jatin Garg on 20/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation

class ArtworkModel: NSObject{
    var artistID: Int?
    var artistName: String?
    var paintingID: Int?
    var title: String?
    var hdImage: String?
    var thumbImage: String?
    var yoc: String?
    var medium: String?
    var material: String?
    var sizeH: Double?
    var sizeW: Double?
    var auctionHouse: String?
    var spINR: Double?
    var spUSD: Double?
    var saleDate: String?
    var nationality: String?
    var mugshot: String?
    var auctionName: String?
    var lot: Int?
    var auctionLocation: String?
    var estMinUSD: Double?
    var estMinINR: Double?
    var estMaxINR: Double?
    var estMaxUSD: Double?
    var status: String?
    var isLiked: Bool
    
    init(dict: [String:Any]) {
        artistID = dict["artist_id"] as? Int
        artistName = dict["artist_name"] as? String
        paintingID = dict["painting_id"] as? Int
        title = dict["title"] as? String
        hdImage = dict["painting_hd_image"] as? String
        thumbImage = dict["painting_thumb_image"] as? String
        yoc = dict["yearofcreation"] as? String
        medium = dict["medium"] as? String
        material = dict["material"] as? String
        sizeH = dict["size_h"] as? Double
        sizeW = dict["size_w"] as? Double
        auctionHouse = dict["auction_house"] as? String
        spINR = dict["sold_price_inr"] as? Double
        spUSD = dict["sold_price_usd"] as? Double
        saleDate = dict["sale_date_2"] as? String
        nationality = dict["nationality"] as? String
        mugshot = dict["mugshot"] as? String
        auctionName = dict["auction_name"] as? String
        auctionLocation = dict["auction_location"] as? String
        lot = dict["lot"] as? Int
        estMaxINR = dict["est_max_inr"] as? Double
        estMaxUSD = dict["est_max_usd"] as? Double
        estMinINR = dict["est_min_inr"] as? Double
        estMinUSD = dict["est_min_usd"] as? Double
        status = dict["sold_price_status"] as? String
        isLiked = (dict["isLiked"] as? Bool) ?? false
    }
    
    static func fetchDetail(forArtID id: Int, completion: @escaping ( ArtworkModel?)->Void){
        let url = kBaseURL + kArtDetail
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token" : userModel?.accessToken ?? "", "painting_id" : id], of: .post) { (result, error) in
            if !kCommonFunctions.handledError(result: result, error: error),
                let data = (result?["data"] as! [[String:Any]]).first{
                completion(ArtworkModel(dict: data))
                
            }else{
                completion(nil)
            }
        }
    }
    
    static func fetchArtWorksFor(artistID: Int, page: Int,completion: @escaping (_ result: [ArtworkModel]?)->Void){
        print("fetching page: \(page)")
        let url = kBaseURL+kArtistWorks
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token":userModel?.accessToken ?? "","page":page,"artist_id":artistID], of: .post) { (result, error) in
            if !kCommonFunctions.handledError(result: result, error: error){
                let data = result!["data"] as! [[String:Any]]
                var temp = [ArtworkModel]()
                for d in data{
                    temp.append(ArtworkModel(dict: d))
                }
                completion(temp)
            }else{
                completion(nil)
            }
        }
    }
    
    static func fetchTopSixWork(forArtist artist: Int, completion: @escaping (_ result: [ArtworkModel]?)->Void){
        let url = kBaseURL+kTop6Works
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token": userModel?.accessToken ?? "","artist_id":artist], of: .post) { (result, error) in
            if !kCommonFunctions.handledError(result: result, error: error){
                let data = result!["data"] as! [[String:Any]]
                var temp = [ArtworkModel]()
                for d in data{
                    temp.append(ArtworkModel(dict: d))
                }
                completion(temp)
            }else{
                completion(nil)
            }
        }
    }
    
    static func fetchTop500Works(forCurrency currency: String, page: Int = 0, completion: @escaping (_ result: [ArtworkModel]?)->Void){
        let url = kBaseURL+kTop500Works
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token": userModel?.accessToken ?? "", "currency" : currency, "page": page], of: .post) { (result, error) in
          callCompletionHandler(result: result, error: error, completion: completion)
        }
    }
    
    class func callCompletionHandler(result: [String:Any]?,error: Error?,completion: @escaping (_ result: [ArtworkModel]?)->Void){
        if !kCommonFunctions.handledError(result: result, error: error){
            guard let data = result?["data"] as? [[String:Any]] else{completion(nil);return}
            var temp = [ArtworkModel]()
            for d in data{
                temp.append(ArtworkModel(dict: d))
            }
            completion(temp)
        }else{
            completion(nil)
        }
    }
    
}

extension ArtworkModel{
    //MARK:- Liking Logic
    typealias completion = (_ success: Bool, _ message: String?)->Void
    
    func toggleLike(_ completion: completion?){
        let url = kBaseURL + kLikeUnlike
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token": userModel?.accessToken ?? "", "painting_id":self.paintingID ?? -1], of: .post) { (result, error) in
            CommonFunctions.shared.debugPrint(dict: result!)
            if(error != nil){
                completion?(false,nil)
            }else{
                switch result?["status"] as? String{
                case .some(let status):
                    if status.lowercased() == "success"{
                        completion?(true,result?["message"] as? String)
                        return
                    }
                    break
                default:
                    break
                }
                
                completion?(false,nil)
            }
        }
        
    }
    
    typealias followCompletion  = (_ success: Bool,_ message: String?) -> Void
    func toggleFollowArtist(_ completion: followCompletion?){
        let url = kBaseURL + kFollowUnfollow
        
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token" : userModel?.accessToken ?? "", "artist_id" : self.artistID ?? ""], of: .post) { (result, error) in
            if error != nil {
                completion?(false, nil)
            }else{
                switch result?["success"] as? Bool{
                case .some(let success):
                    if success{
                        completion?(true,result?["message"] as? String)
                        return
                    }
                default:
                    break
                }
                
                completion?(false,nil)
            }
        }
    }
}
