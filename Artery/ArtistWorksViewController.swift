//
//  ArtistWorksViewController.swift
//  Artery
//
//  Created by Jatin Garg on 23/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import SDWebImage

class ArtistWorksViewController: UIViewController{
    let cellID = "workCVC"
    let headerID = "headerSection"
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var topSixWorks = [ArtworkModel]()
    var artworks = [ArtworkModel]()
    var artist: ArtistModel?
    var page = 1
    weak var delegate: ArtistMainViewController?
    var canLoadMore = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let artistName = artist?.name{
            headerTitle.text = "\(artistName)\n Works"
        }
        loadTopSixWorks()
        loadData(forPage: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func loadTopSixWorks(){
        if let id = artist?.id{
            ArtworkModel.fetchTopSixWork(forArtist: id , completion: { (model) in
            guard let model = model else {return}
                for work in model{
                    self.topSixWorks.append(work)
                }
                OperationQueue.main.addOperation {
                    self.collectionView.reloadData()
                }
            })
        }
        
    }
    
    func loadData(forPage page: Int?){
        if let id = artist?.id{
            var pageLimit: Int!
            if page == nil{
                pageLimit = self.page
            }else{
                pageLimit = page!
            }
            CircularProgressView.startAnimating()
            ArtworkModel.fetchArtWorksFor(artistID: id, page: pageLimit, completion: { (model) in
                CircularProgressView.stopAnimating()
                guard let model = model else { self.canLoadMore = false;return}
                if model.count == 0{self.canLoadMore = false}
                for work in model{
                    self.artworks.append(work)
                }
                
                OperationQueue.main.addOperation {
                    self.collectionView.reloadData()
                }
            })
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        delegate?.childVCDismissed(vc: self)
    }
}

extension ArtistWorksViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section{
        case 0:
            return topSixWorks.count
        default:
            return artworks.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        let imageView = cell.viewWithTag(1) as! JGImageView
        var thumbImage: String?
        
        if indexPath.section == 0{
            thumbImage = topSixWorks[indexPath.item].thumbImage
            //top 6 works
        }else{
            thumbImage = artworks[indexPath.item].thumbImage
        }
        if thumbImage != nil{
            imageView.sd_setImage(with: URL(string: thumbImage!))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = UIScreen.main.bounds.width/3
        let width = height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == 1{
            //all works 
            if indexPath.item == artworks.count - 1 && self.canLoadMore{
                //just rendered last cell, ask for more
                self.page += 1
                loadData(forPage: self.page)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var selectedItem : ArtworkModel
        if indexPath.section == 0{
            selectedItem = topSixWorks[indexPath.item]
        }else{
            selectedItem = artworks[indexPath.item]
        }
        selectedItem.mugshot = artist?.mugshot
        let vc = storyboard?.instantiateViewController(withIdentifier: "artMain") as! ArtViewController
        vc.artWork = selectedItem
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let v = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerID, for: indexPath)
        let label = v.viewWithTag(1) as! UILabel
        if indexPath.section == 0{
            v.backgroundColor = UIColor(colorLiteralRed: 181/255, green: 142/255, blue: 99/255, alpha: 1)
            label.text = "TOP 6"
            label.textColor = .white
        }else{
            v.backgroundColor = .white
            label.text = "WORKS"
            label.textColor = .gray
        }
        return v
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 30)
    }
    
    
    
}
