//
//  BorderedTextField.swift
//  Artery
//
//  Created by Jatin Garg on 16/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class BorderedTextField: UITextField{
    var borderColor: UIColor = .white
    var borderWidth: CGFloat = 1
    var placeHolderColor: UIColor = .white
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        borderStyle = .line
        if let text = placeholder{
            let attributedText = NSAttributedString(string: text, attributes: [NSForegroundColorAttributeName: placeHolderColor])
            self.attributedPlaceholder = attributedText
        }
    }
}
