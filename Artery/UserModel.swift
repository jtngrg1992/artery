//
//  UserModel.swift
//  Artery
//
//  Created by Jatin Garg on 18/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation

class UserModel: NSObject{
    var accessToken: String?
    var active: Bool?
    var address: String?
    var category: UserCategory?
    var createdDate: String?
    var prefCurrency: String?
    var email: String?
    var fbHandle: String?
    var gplusHandle: String?
    var id: Int?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var photo: String?
    var type: Int?
    var updatedDate: String?
    var verificationToken: String?
    var walkthough : Bool?
    
    init(dict: Dictionary<String,Any>) {
        accessToken = dict["access_token"] as? String
        active = dict["active"] as? Bool
        address = dict["address"] as? String
        if let cat = dict["category"] as? String{
            category = UserCategory(rawValue: cat)
        }
        
        createdDate = dict["created_date"] as? String
        prefCurrency = dict["currency_preffered"] as? String
        email = dict["email"] as? String
        fbHandle = dict["fb_handle"] as? String
        firstName = dict["first_name"] as? String
        gplusHandle = dict["gplus_handle"] as? String
        id = dict["id"] as? Int
        lastName = dict["last_name"] as? String
        phone = dict["phone"] as? String
        photo = dict["photo"] as? String
        type = dict["type"] as? Int
        updatedDate = dict["updated_date"] as? String
        verificationToken = dict["verification_token"] as? String
        walkthough = dict["walkthrough"] as? Bool
    }
    
    
    typealias followCompletion = ([ArtistModel]?) -> Void
    func getFollowedArtists(completion: @escaping followCompletion){
        let url = kBaseURL + kFollowedArtists
        
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token" : self.accessToken ?? ""], of: .post) { (result, error) in
            if error != nil {
                completion(nil)
            }else{
                switch result?["data"] as? [[String:Any]]{
                case .some(let artists):
                    
                    var temp = [ArtistModel]()
                    for artist in artists{
                        temp.append(ArtistModel(dict: artist))
                    }
                    
                    completion(temp)
                    return
                default:
                    break
                }
                
                completion(nil)
            }
        }
    }
    
    typealias likedCompletion = ([ArtworkModel]?) -> Void
    
    func getLikedPaintings(_ completion: @escaping likedCompletion){
        let url = kBaseURL + kLikedPaintings
        
        RequestHandler.shared.makeRequest(with: URL(string: url), using: ["access_token" : self.accessToken ?? ""], of: .post) { (result, error) in
            if error != nil {
                completion(nil)
            }else{
                switch result?["data"] as? [[String:Any]]{
                case .some(let artists):
                    
                    var temp = [ArtworkModel]()
                    for artist in artists{
                        temp.append(ArtworkModel(dict: artist))
                    }
                    
                    completion(temp)
                    return
                default:
                    break
                }
                
                completion(nil)
            }
        }
    }
}
