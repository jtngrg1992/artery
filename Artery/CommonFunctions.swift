//
//  CommonFunctions.swift
//  Artery
//
//  Created by Jatin Garg on 15/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class CommonFunctions: NSObject{
    static let shared = CommonFunctions()
    
    
    func showErrorMessage(withTitle title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        OperationQueue.main.addOperation {
            self.getVisibleViewController()?.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func handledError(result: [String:Any]?,error: Error?)->Bool{
        var message: String?
        if error != nil{
            message = error?.localizedDescription
        }else{
            if let error = result?["error"] as? Bool, error == true{
                message = kErrorMessage
            }else if let success = result?["success"] as? Bool, success == false{
                 message = result!["message"] as? String
            }
        }
        
        if message != nil{
            showErrorMessage(withTitle: kAlertTitle, message: message!)
        }
        return message != nil
        
    }
    
    func getVisibleViewController()->UIViewController?{
        if let keyWindow = UIApplication.shared.keyWindow, let rootVc = keyWindow.rootViewController{
            if let nav = rootVc as? UINavigationController{
                return nav.visibleViewController
            }
            if let tab = rootVc as? UITabBarController{
                return tab.selectedViewController
            }
            if let presentedVc = rootVc.presentedViewController{
                return presentedVc
            }
            return rootVc
        }
        return nil
    }
    
    func saveImageToCache(imageData: Data, withName name: String, in folder: String?=nil){
        guard let cache = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else {return}
        var kFolder: String!
        if folder == nil{
            kFolder = "/images"
        }else{
            kFolder = folder!
        }
        let saveDirectory = cache + "/\(kFolder!)/"
        if !FileManager.default.fileExists(atPath: saveDirectory){
            do{
                try FileManager.default.createDirectory(atPath: saveDirectory, withIntermediateDirectories: false, attributes: nil)
            }catch let error {
                print("unable to create directory: \(error.localizedDescription)")
            }
        }
        let finalDirectory = saveDirectory+name
        if !FileManager.default.fileExists(atPath: finalDirectory){
            FileManager.default.createFile(atPath: finalDirectory, contents: imageData, attributes: nil)
        }
    }
    
    func retrieveImageFromCache(withName name: String)->UIImage?{
        guard let cache = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first else {return nil}
        let location = cache + "/images/\(name)"
        let fileURL = URL(fileURLWithPath: location)
        if FileManager.default.fileExists(atPath: fileURL.path){
            if let data = try? Data(contentsOf: fileURL), let image = UIImage(data: data){
                return image
            }else{
                return nil
            }
            
        }
        return nil
    }
    
    func getHomeStoryBoard()->UIStoryboard{
        let sb = UIStoryboard(name: "Home", bundle: nil)
        return sb
    }
    
    func debugPrint(dict: [String: Any]?){
        guard let dict = dict else { return }
        
        do{
          let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                print(string)
            }
            
        }catch{
            
        }        
    }
}
