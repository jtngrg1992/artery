//
//  ArtViewController.swift
//  Artery
//
//  Created by Jatin Garg on 23/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Font_Awesome_Swift

class ArtViewController: UIViewController {
    @IBOutlet weak var addCommentTF: UITextView!
    @IBOutlet weak var workDesc: UILabel!
    @IBOutlet weak var workTitle: UILabel!
    @IBOutlet weak var workImage: JGImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistImage: JGImageView!
    @IBOutlet weak var followBtn: UIButton!
    
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var likeBtn: UIButton!
    var artWork: ArtworkModel?
    var artID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if artWork != nil {
            setupViews()
        }else if artID != nil {
            CircularProgressView.startAnimating()
            ArtworkModel.fetchDetail(forArtID: artID!) { art in
                DispatchQueue.main.async {
                    CircularProgressView.stopAnimating()
                    self.artWork = art
                    self.setupViews()
                }
            }
        }
        
        artistImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(artistTapped)))
        artistName.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(artistTapped)))
    }
    
    @IBAction func backTapped(_ sender : UIButton){
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func shareTapped(_ sender: Any) {
    }
    
    @IBAction func likeTapped(_ sender: Any) {
        artWork?.toggleLike{ (success,message) in
            DispatchQueue.main.async {
                if !success{
                    iToast.makeText("Unable to complete action, please try again later").show()
                }else if let message = message{
                    iToast.makeText(message).show()
                }else{
                    iToast.makeText("Action completed!").show()
                }
            }
        }
    }
    
    @IBAction func followTapped(_ sender: Any) {
        artWork?.toggleFollowArtist{(success,message) in
            DispatchQueue.main.async {
                if success{
                    guard let msg = message else {
                        iToast.makeText("Action completed!").show()
                        return
                    }
                    iToast.makeText(msg.capitalized).show()
                }else{
                    iToast.makeText("Failed to complete the action, please try again later").show()
                }
            }
        }
    }
    
    func artistTapped(tap: UITapGestureRecognizer){
        performSegue(withIdentifier: "artistMain", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! ArtistMainViewController
        destination.artistID = artWork?.artistID
    }
    
    func setupViews(){
        guard let art = artWork else {return}
        workImage.imageURL = art.hdImage
        workTitle.text = art.title ?? ""
        artistName.text = art.artistName ?? ""
        artistImage.imageURL = art.mugshot
        likeBtn.setFAIcon(icon: FAType.FAHeart , iconSize: 25, forState: .normal)
        shareBtn.setFAIcon(icon: .FAShareAlt, iconSize: 25,forState: .normal)
        
        let medium = art.medium ?? ""
        let material = art.material ?? ""
        var materialText = ""
        if medium != "" && material != ""{
            materialText = medium.capitalizingFirstLetter() + " on " + material + "\n"
        }
        let sizeH = art.sizeH ?? 0
        let sizeW = art.sizeW ?? 0
        
        var dimText = ""
        if sizeW != 0 && sizeH != 0{
            dimText = "\(sizeW) X \(sizeH) \n"
        }
        
        var auctionText = ""
        let auctionHouse = art.auctionHouse ?? ""
        if auctionHouse != ""{
            auctionText = "Sold by \(auctionHouse) \n"
        }
        
        workDesc.text = materialText + dimText + auctionText
        
    }
}
