//
//  ViewController.swift
//  Artery
//
//  Created by Jatin Garg on 15/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import FBSDKLoginKit


class ViewController: UIViewController {
    
    
    @IBAction func googleLoginTapped(_ sender: Any) {
        
    }
    
    @IBAction func fbLoginTapped(_ sender: Any) {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if error == nil{
                if result!.grantedPermissions.contains("email"){
                    self.getData()
                }else{
                    //show no email verified error message
                    kCommonFunctions.showErrorMessage(withTitle: kAlertTitle, message: kFBEmailNAMessage)
                }
            }
        }
    }
    
    func getData(){
        CircularProgressView.startAnimating()
        if FBSDKAccessToken.current() != nil{
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) in
                CircularProgressView.stopAnimating()
                if error == nil{
                    let userLoginInfo = UserLoginInfo(FromFBDict: result as! [String:Any])
                    kRequestHandler.makeRequest(with: URL(string: kBaseURL+kFbSignup), using: userLoginInfo.toDict(), of: .post, { (result, error) in
                        if !kCommonFunctions.handledError(result: result, error: error){
                            //login successfull
                            if var data = result?["data"] as? Dictionary<String,Any>{
                                data["updated_date"] = nil
                                UserDefaults.standard.set(data, forKey: kUserLoginInfo)
                                userModel = UserModel(dict: data)
                                //jump to home
                                OperationQueue.main.addOperation {
                                    self.present(kCommonFunctions.getHomeStoryBoard().instantiateInitialViewController()!, animated: true, completion: nil)
                                }
                            }
                            else{
                                guard let token = result?["access_token"] as? String else{return}
                                UserDefaults.standard.set(["access_token":token], forKey: kUserLoginInfo)
                                userModel = UserModel(dict: ["access_token":token])
                                OperationQueue.main.addOperation {
                                    self.present(kCommonFunctions.getHomeStoryBoard().instantiateInitialViewController()!, animated: true, completion: nil)
                                }
                            }
                            
                            
                        }
                    })
                }
            })
        }
    }
    @IBAction func loginTapped(_ sender: Any) {
    }
    
    @IBAction func signupTapped(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        RequestHandler.shared.makeRequest(with: URL(string: "https://jsonplaceholder.typicode.com/posts"), using: nil, of: .post) { (result, error) in
            
        }
    }
    
    
    
    
}

