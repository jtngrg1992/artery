//
//  GridCollectionViewController.swift
//  Artery
//
//  Created by Jatin Garg on 22/10/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

//Intended to display either the followed artists
//or the liked paintings


enum GridPresentationMode{
    case likedPaintings, followedArtists
    
    func stringTitle()->String{
        switch self{
        case .likedPaintings:
            return "paintings liked"
        case .followedArtists:
            return "artists followed"
        }
    }
}
class GridCollectionViewController: UIViewController{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    public var displayMode: GridPresentationMode?
    public var overlayAnimatedView: UIView?
    public var followedArtists: [ArtistModel]?
    public var likedPaintings: [ArtworkModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = (displayMode != nil && displayMode == .likedPaintings) ? "Paintings Liked" : "Artists Followed"
        titleLabel.text = titleLabel.text?.uppercased()
    }
    
    @IBAction func backTapped(_ sender: UIButton){
        _=navigationController?.popViewController(animated: true)
    }
}

extension GridCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        guard let imageView = cell.viewWithTag(1) as? UIImageView else { fatalError() }
        
        var imageURL: String?
        switch displayMode!{
        case .followedArtists:
            imageURL = followedArtists?[indexPath.item].mugshot
        case .likedPaintings:
            imageURL = likedPaintings?[indexPath.item].thumbImage
        }
        
        guard let image = imageURL else { fatalError() }

        imageView.sd_setImage(with: URL(string: image))
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //jump to details
        guard displayMode != nil else { return }
        
        switch displayMode!{
        case .followedArtists:
            //fetch artist details and jump to artist detail page
            guard let artistID = followedArtists?[indexPath.item].id else { return }
            let sb = UIStoryboard(name: "Browse", bundle: nil)
            let artistMainVC = sb.instantiateViewController(withIdentifier: "artistMain") as! ArtistMainViewController
            artistMainVC.artistID = artistID
            navigationController?.pushViewController(artistMainVC, animated: true)
            
        case .likedPaintings:
            //jump to painting detail
            guard let artId = likedPaintings?[indexPath.item].paintingID else { return }
            let sb = UIStoryboard(name: "Browse", bundle: nil)
            let artMainVc = sb.instantiateViewController(withIdentifier: "artMain") as! ArtViewController
            artMainVc.artID = artId
            navigationController?.pushViewController(artMainVc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let mode = displayMode else {
            fatalError()
        }
        
        switch mode{
        case .followedArtists:
            guard let followed = followedArtists else { fatalError() }
            return followed.count
        case .likedPaintings:
            guard let liked = likedPaintings else { fatalError() }
            return liked.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 1, 0, 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let numOfItemsPerRow: CGFloat = 3
        let effectiveScreenWidth = (screenWidth/numOfItemsPerRow)-2
        
        return CGSize(width: effectiveScreenWidth, height: effectiveScreenWidth)
    }
}
