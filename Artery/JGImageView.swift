import UIKit
import Font_Awesome_Swift
class JGImageView: UIImageView{
    var imageURL: String?{
        didSet{
            setImage(from: imageURL)
        }
    }
    
    func setImage(from url: String?){
        if let imageurl = url{
            self.sd_setImage(with: URL(string: imageurl))
        }else{
            self.image = #imageLiteral(resourceName: "logo-potrait")
        }
    }
    func loadImage(from url: String?){
        self.image = nil
        self.contentMode = .scaleAspectFill
        func setErrorImage(){
            OperationQueue.main.addOperation {
                self.backgroundColor = .black
                self.contentMode = .scaleAspectFit
                let img = UIImage(bgIcon: FAType.FATerminal, topIcon: FAType.FAExclamationTriangle)
                self.image = img
            }
            
        }
        
        if let imageURL = url, let convertedURL = URL(string: imageURL){
            if let cacheImage = kCommonFunctions.retrieveImageFromCache(withName: convertedURL.lastPathComponent){
                self.image = cacheImage
                return
            }
            URLSession.shared.dataTask(with: convertedURL){data,response,error in
                if error != nil || data == nil{
                    setErrorImage()
                    return
                }
                //we have the image data
                if let downloadedIMG = UIImage(data: data!){
                    //compare incoming and outgoing urls
                    if let responseURL = response?.url?.absoluteString, self.imageURL == responseURL{
                        //clear to set the image
                        kCommonFunctions.saveImageToCache(imageData: data!, withName: URL(string: responseURL)!.lastPathComponent)
                        OperationQueue.main.addOperation {
                            self.image = downloadedIMG
                        }
                    }
                }else{
                    setErrorImage()
                }
                }.resume()
        }else{
            //set error image
            setErrorImage()
        }
    }
    
}
