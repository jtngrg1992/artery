//
//  BrowseSectionHeader.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import Font_Awesome_Swift

@objc protocol BrowseSectionHeaderDelegate: class{
    @objc optional func dropDownPressed()
    @objc optional func searchPressed()
}

class BrowseSectionHeader: UIView{
    
    weak var delegate: BrowseSectionHeaderDelegate?
    @IBOutlet weak var letterLabel: UILabel!
    @IBOutlet weak var dropBtn: UIButton!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var browseLabel: UILabel!
    
    @IBOutlet weak var letterContainerView: UIView!
    @IBAction func searchTapped(_ sender: Any) {
        delegate?.searchPressed?()
    }
    
    @IBAction func dropTapped(_ sender: Any) {
        delegate?.dropDownPressed?()
    }
    
    var borderColor = UIColor.gray
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup(){
        let nib = Bundle.main.loadNibNamed("BrowseSectionHeader", owner: self, options: nil)?.first as! UIView
        nib.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        addSubview(nib)
        nib.translatesAutoresizingMaskIntoConstraints = false
        nib.topAnchor.constraint(equalTo: topAnchor).isActive = true
        nib.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        nib.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        nib.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        searchBtn.setFAIcon(icon: .FASearch, iconSize: 20, forState: .normal)
        letterContainerView.layer.borderColor = borderColor.cgColor
        letterContainerView.layer.borderWidth = 2
        dropBtn.layer.borderWidth = 2
        dropBtn.layer.borderColor = borderColor.cgColor
        dropBtn.setFAIcon(icon: .FAArrowDown, iconSize: 20, forState: .normal)
        
        
        
    }
}
