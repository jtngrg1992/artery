//
//  PDBHomeViewController.swift
//  Artery
//
//  Created by Jatin Garg on 25/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit

class PDBHomeViewController: UIViewController{
    enum headerMode{
        case visible
        case notVisible
    }
    enum vcMode{
        case featured
        case alphabetical
    }
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headerCarousel: UIView!
    @IBOutlet weak var dropDownBtn: UIButton!
    var headerVisibility: headerMode = .visible
    var headerHeight: CGFloat = UIScreen.main.bounds.height/2.8
    var dataMode: vcMode = .featured
    var selectedAlphabet: String?
    var allArtists = [ArtistModel]()
    var images = [String]()
    var timer: Timer!
    var beginReverseTracking = false
    var sectionHeader: BrowseSectionHeader!
    var featuredArtists = [ArtistModel](){
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }
    }
    
    func toggleHeader(){
        tableView.beginUpdates()
        var height: CGFloat = 0
        if headerVisibility == .visible{
            headerVisibility = .notVisible
            dropDownBtn.setFAIcon(icon: .FAAngleUp, iconSize: 25, forState: .normal)
        }else{
            dropDownBtn.setFAIcon(icon: .FAAngleDown, iconSize: 25, forState: .normal)
            height = headerHeight
            headerVisibility = .visible
        }
        
        tableView.tableHeaderView?.frame.size.height = height
        tableView.endUpdates()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backBtn.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        dropDownBtn.setFAIcon(icon: .FAAngleDown, iconSize: 25, forState: .normal)
        dropDownBtn.layer.borderWidth = 4
        dropDownBtn.tintColor = primaryAccentColor
        dropDownBtn.layer.borderColor = primaryAccentColor.cgColor
        tableView.tableHeaderView?.frame.size.height = headerHeight
        loadCarousel()
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(timeFired), userInfo: nil, repeats: true)
        loadData(forAlphabet: nil)
    }
    
    func loadData(forAlphabet alpha: String?){
        CircularProgressView.startAnimating()
        if dataMode == .featured || alpha == nil{
            ArtistModel.getFeaturedArtists { (model) in
                CircularProgressView.stopAnimating()
                if model != nil{
                    self.featuredArtists = model!
                }
                
            }
        }else if dataMode == .alphabetical && alpha != nil{
            if allArtists.count == 0{
                ArtistModel.getTop50Artists(usingCurrency: "USD", completion: { (model) in
                    CircularProgressView.stopAnimating()
                    if model != nil{
                        self.allArtists = model!
                        self.featuredArtists = model!.filter({$0.name?.hasPrefix(alpha!) == true})
                    }
                })
            }else{
                CircularProgressView.stopAnimating()
                self.featuredArtists = allArtists.filter({$0.name?.hasPrefix(alpha!) == true})
            }
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
    }
    
    
    @IBAction func showMoreArtists(){
        toggleHeader()
    }
    
    func timeFired(){
        let scrollView = headerCarousel.viewWithTag(1) as! UIScrollView
        let width = UIScreen.main.bounds.width
        if scrollView.contentOffset.x < scrollView.contentSize.width-width && !beginReverseTracking{
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                scrollView.contentOffset.x += width
            }, completion: nil)
        }else if !beginReverseTracking{
            beginReverseTracking = true
        }
        
        
        if scrollView.contentOffset.x > 0 && beginReverseTracking{
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                scrollView.contentOffset.x -= width
            }, completion: nil)
        }else if beginReverseTracking{
            beginReverseTracking = false
        }
        
        let page = Int(scrollView.contentOffset.x/width)
        let pageControl = headerCarousel.viewWithTag(2) as! UIPageControl
        pageControl.currentPage = page
    }
    
    
    func loadCarousel(){
        for i in 0...5{
            let image = getRandomWork()
            let width = UIScreen.main.bounds.width
            let offSet = width * CGFloat(i)
            
            let imageView = UIImageView(frame: CGRect(x: offSet, y: 0, width: width, height: headerCarousel.frame.height))
            imageView.contentMode = .scaleAspectFill
            imageView.image = image
            imageView.clipsToBounds = true
            let scroll = (headerCarousel.viewWithTag(1) as! UIScrollView)
            let pageController = headerCarousel.viewWithTag(2) as! UIPageControl
            scroll.delegate = self
            scroll.insertSubview(imageView, belowSubview: pageController)
            scroll.contentSize.width = CGFloat(i+1) * width
        }
        
        
    }
    
    func getRandomWork()->UIImage?{
        let images = Bundle.main.paths(forResourcesOfType: "jpg", inDirectory: nil)
        let random = arc4random_uniform(UInt32(images.count-1))
        if let data = try? Data(contentsOf: URL(fileURLWithPath: images[Int(random)])), let image = UIImage(data: data){
            if self.images.index(of: images[Int(random)]) != nil{
                return getRandomWork()
            }
            self.images.append(images[Int(random)])
            return image
        }
        return nil
    }
    func backTapped(){
        _=navigationController?.popViewController(animated: true)
    }
}

extension PDBHomeViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        sectionHeader = BrowseSectionHeader(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        if selectedAlphabet != nil{
            sectionHeader.letterLabel.text = selectedAlphabet
        }
        sectionHeader.delegate = self
        return sectionHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let artist = featuredArtists[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let imageView = cell?.viewWithTag(1) as! UIImageView
        let catLabel = cell?.viewWithTag(3) as! UILabel
        let nameLabel = cell?.viewWithTag(2) as! UILabel
        
        imageView.sd_setImage(with: URL(string: artist.mugshot!))
        catLabel.text = artist.categoryName?.capitalized
        nameLabel.text = artist.name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return featuredArtists.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = tableView.frame.height - headerCarousel.frame.height - 50
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Browse", bundle: nil)
        let artistMainVC = sb.instantiateViewController(withIdentifier: "artistMain") as! ArtistMainViewController
        artistMainVC.artistID = featuredArtists[indexPath.row].id!
        navigationController?.pushViewController(artistMainVC, animated: true)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView is UITableView{
            
        }else{
            let offset = scrollView.contentOffset.x
            let width = UIScreen.main.bounds.width
            let page = offset/width
            let pageControl = headerCarousel.viewWithTag(2) as! UIPageControl
            pageControl.currentPage = Int(page)
        }
    }
    
}

extension PDBHomeViewController: BrowseSectionHeaderDelegate, AlphabetPickerDelegate{
    func dropDownPressed() {
        if view.subviews.filter({$0 as? AlphabetPicker != nil}).count != 0{
            return
        }
        let height: CGFloat = view.frame.height * 2/5
        let yorigin = view.bounds.height
        let alphabetPicker = AlphabetPicker(frame: CGRect(x: 0, y: yorigin , width: view.frame.width, height: height))
        view.addSubview(alphabetPicker)
        alphabetPicker.delegate = self
        alphabetPicker.frame.origin.y = view.frame.height
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            alphabetPicker.frame.origin.y -= height
        }, completion: nil)
    }
    
    func alphaPicked(alpha: String) {
        sectionHeader.letterLabel.text = alpha
        dataMode = .alphabetical
        selectedAlphabet = alpha
        loadData(forAlphabet: alpha)
        let picker = view.subviews.filter({$0 as? AlphabetPicker != nil}).first!
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.8, options: .curveEaseInOut, animations: {
            picker.frame.origin.y += picker.frame.height
        }, completion: {success in
            picker.removeFromSuperview()
            
        })
    }
    
}


