//
//  Constants.swift
//  Artery
//
//  Created by Jatin Garg on 15/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit


//MARK: Colors
let primaryAccentColor: UIColor = UIColor(colorLiteralRed: 213/255, green: 80/255, blue: 79/255, alpha: 1)
let secondaryAccentColor: UIColor = UIColor(colorLiteralRed: 219/255, green: 219/255, blue: 220/255, alpha: 1)
let fbColor: UIColor = UIColor(colorLiteralRed: 59/255, green: 91/255, blue: 179/255, alpha: 1)
let primaryBtnTint: UIColor = UIColor(colorLiteralRed: 138/255, green: 138/255, blue: 141/255, alpha: 1)

//MARK: API
let kBaseURL = "http://arteryindia.com:8000"
let kEmailSignup = "/auth/signup"
let kFbSignup = "/auth/facebook"
let kGSignup = "/auth/gplus"
let kLogin = "/auth/login"
let kTop500Works = "/api/top500Work"
let kTop50Artists = "/api/top50Artists"
let kLikeUnlike = "/me/changeLike"
let kFollowUnfollow = "/me/changeFollow"
let kartistInfo = "/api/artistInfo"
let kArtistRank = "/api/artistRank"
let kArtistWorks = "/api/artistPaintings/paging"
let kTop6Works = "/api/top6works"
let kFeaturedArtists = "/api/featuredArtists"
let kFollowedArtists = "/me/getFollow/"
let kLikedPaintings = "/me/getLikes"
let kGetInsights = "/api/getInsights/"
let kGetAllInsights = "/api/getAllInsights/"
let kArtDetail = "/api/artDetail"

//MARK: User Defaults
let kUserLoginInfo = "userinfo"

//MARK: Messages
let kErrorMessage = "We are experiencing some issues, please try again later"
let kFBEmailNAMessage = "Sorry, you must verify your email with facebook to use facebook login"

//MARK: Titles
let kAlertTitle = "Artery"

//MARK: Common Types
enum LoginType: String{
    case facebook = "facebook"
    case google = "google"
}

enum UserCategory: String{
    case collector = "COLLECTOR"
}
//MARK: Static class instances
let kCommonFunctions = CommonFunctions.shared
let kRequestHandler = RequestHandler.shared

