//
//  UserLoginInfo.swift
//  Artery
//
//  Created by Jatin Garg on 17/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import Foundation

class UserLoginInfo: NSObject{
    var firstName: String?
    var lastName: String?
    var profilePic: String?
    var loginType: LoginType?
    var loginID: String?
    var email: String?
    
    init(dict: [String:Any]) {
        email = dict["email"] as? String
        firstName = dict["firstName"] as? String
        lastName = dict["lastName"] as? String
        profilePic = dict["profilePic"] as? String
        if let type = dict["loginType"] as? String{
            loginType = LoginType(rawValue: type)
        }
        loginID = dict["loginID"] as? String
    }
    
    init(fn:String?,ln: String?,profilePic: String?,loginType: LoginType?,loginID: String?,email: String?) {
        self.firstName = fn
        self.lastName = ln
        self.profilePic = profilePic
        self.loginID = loginID
        self.loginType = loginType
        self.email = email
    }
    
    init(FromFBDict dict: Dictionary<String,Any>){
        email = dict["email"] as? String
        firstName = dict["first_name"] as? String
        lastName = dict["last_name"] as? String
        profilePic = ((dict["picture"] as? [String:Any])?["data"] as? [String:Any])?["url"] as? String
        loginType = .facebook
        loginID = dict["id"] as? String
    }
    
    public func toDict()->[String:Any]{
        let dict = ["firstName":self.firstName,"lastName": self.lastName,"profilePic": profilePic,
                    "loginType": loginType?.rawValue,"loginID": loginID, "email":self.email]
        return dict
    }
    
    public func saveToUD(){
        UserDefaults.standard.set(self.toDict(), forKey: kUserLoginInfo)
        UserDefaults.standard.synchronize()
    }
}
