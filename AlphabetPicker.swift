//
//  AlphabetPicker.swift
//  Artery
//
//  Created by Jatin Garg on 26/07/17.
//  Copyright © 2017 Jatin Garg. All rights reserved.
//

import UIKit
import CoreData


@objc protocol AlphabetPickerDelegate: class{
    @objc optional func alphaPicked(alpha: String)
}

class AlphabetPicker: UIView, UIPickerViewDelegate, UIPickerViewDataSource{
    
    var pickerData = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    weak var delegate: AlphabetPickerDelegate?
    lazy var doneView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        let b = UIButton(type: .system)
        b.setTitle("Done", for: .normal)
        b.titleLabel?.font = UIFont(name: "Hero", size: 18)
        b.translatesAutoresizingMaskIntoConstraints = false
        v.addSubview(b)
        b.centerYAnchor.constraint(equalTo: v.centerYAnchor).isActive = true
        b.rightAnchor.constraint(equalTo: v.rightAnchor).isActive = true
        b.tintColor = .black
        b.addTarget(self, action: #selector(donePressed), for: .touchUpInside)
        return v
    }()
    
    lazy var pickerView: UIPickerView = {
        let p = UIPickerView()
        p.delegate = self
        p.dataSource = self
        p.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        return p
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        backgroundColor = UIColor.white
        addSubview(pickerView)
        addSubview(doneView)
        doneView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        doneView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        doneView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        doneView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.topAnchor.constraint(equalTo: doneView.bottomAnchor).isActive = true
        pickerView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        pickerView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        pickerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
    }
    
    func donePressed(){
        let alpha = pickerData[pickerView.selectedRow(inComponent: 0)]
        delegate?.alphaPicked?(alpha: alpha)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleString = pickerData[row]
        let color = row == pickerView.selectedRow(inComponent: component) ? primaryAccentColor : UIColor.white
        let attrString = NSAttributedString(string: titleString, attributes: [NSForegroundColorAttributeName : color, NSFontAttributeName : UIFont(name: "Hero", size: 18)!])
        return attrString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadAllComponents()
    }
    
    
}
